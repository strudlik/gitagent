<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Entity\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class AgentController extends Controller
{

    /**
    * @Route("/", name="homepage")
    */
    public function index()
    {
        $queries = $this->getDoctrine()->getRepository(Agent::class)->findAll();
        return $this->render('view/index.html.twig', array(
          "count" => count($queries)
        ));
    }

    /**
    * @Route("/search", name="search")
    * Method({"POST"})
    */
    public function id(Request $request)
    {

      if($request->getMethod() == "GET"){

        return $this->redirectToRoute('homepage');

      }else{

        $em = $this->getDoctrine()->getManager();

        $agent = new Agent();

        $agent->setQuery($request->request->get('name'));
        $agent->setDate(new \DateTime("now"));
        $agent->setIp($request->request->get('ip'));

        $em->persist($agent);
        $em->flush();

        return $this->render('view/repo.html.twig',array(
          "repo" => $request->request->get('name')
        ));

      }

    }

    /**
    * @Route("/admin/delete", name="delete_records")
    */

    public function delete(Request $request){

      if($request->getMethod() == "POST"){
        $session = new Session();
          if(strlen($session->get('hash')) == 16){

           $em = $this->getDoctrine()->getManager();
           $qb = $em->createQueryBuilder();

            $dt = new \DateTime("now");
            $dt->modify('- ' . $request->request->get('_hours') . ' hour');

            $qb->delete('App\Entity\Agent', 'u')
             ->where('u.date < :date')
             ->setParameter('date', $dt);

             $out = $qb->getQuery();
             $out->getResult();

            return $this->redirectToRoute('admin_page');

          }
      }else{
        return $this->redirectToRoute('admin_page');
      }

    }

    /**
    * @Route("/login", name="admin_login")
    */

    public function admin(){

      $session = new Session();
      if(strlen($session->get('hash')) == 16){

      return $this->redirectToRoute('admin_page');

      }else{

      return $this->render('view/admin-login.html.twig');

      }

    }

    /**
    * @Route("/validate", name="validate")
    */

    public function validate(Request $request){

      if($request->getMethod() == "POST"){

        $em = $this->getDoctrine()->getManager();
        $valid_users = $em->getRepository(User::class)
        ->findBy(array(
          'user' => $request->request->get('_user'),
          'pass' => md5($request->request->get('_pass') . 'git4g3nt')
        ));

        $valid_users = count($valid_users);

        if($valid_users == 0){
          return $this->redirectToRoute('admin_login');
        }else{

          $session = new Session();
          $session->set('hash', substr(md5($request->request->get('_pass')),0,-16));

          return $this->redirectToRoute('admin_page');
        }

      }else{

        return $this->redirectToRoute('admin_login');
        
      }

    }


    /**
    * @Route("/admin", name="admin_page")
    */

    public function admin_page(){
      $session = new Session();
      if(strlen($session->get('hash')) == 16){
        $em = $this->getDoctrine()->getManager();
        $last_searches = $this->getDoctrine()->getRepository(Agent::class)->findBy(array(),array("date" => "DESC"),10,0);
        $total = $this->getDoctrine()->getRepository(Agent::class)->findAll();

        $pages = 0;
        $pages_array = [];

        if( round( (count($total) / 10) ) > ( count($total) / 10 ) ){

          $pages = ( (count($total) / 10) + 1);

        }else{

          $pages = (count($total) / 10);

        }

        for($i = 1; $i <= $pages; $i++){
          $pages_array[] = ['num' => $i];
        }

        if(count($last_searches) == 0 ) return $this->redirectToRoute('admin_page');

        return $this->render('view/admin-page.html.twig', array("queries" => $last_searches, "total" => count($total), "pages" => $pages_array));

      }else{
        return $this->redirectToRoute('admin_login');
      }
    }

    /**
    * @Route("/admin/{id}", name="admin_page_paginator")
    */

    public function admin_page_paginator($id){
      $session = new Session();
      if(strlen($session->get('hash')) == 16){
        if(is_numeric($id)){

          $id = $id - 1;

          $show = 10;
          $page = 10 * $id;
          $pages = [];

          $em = $this->getDoctrine()->getManager();
          $last_searches = $this->getDoctrine()->getRepository(Agent::class)->findBy(array(),array("date" => "DESC"),$show,$page);
          $total = $this->getDoctrine()->getRepository(Agent::class)->findAll();

          $pages = 0;
          $pages_array = [];

          if( round( (count($total) / 10) ) > ( count($total) / 10 ) ){

            $pages = ( (count($total) / 10) + 1);

          }else{

            $pages = (count($total) / 10);

          }

          for($i = 1; $i <= $pages; $i++){
            $pages_array[] = ['num' => $i];
          }

          if(count($last_searches) == 0 ) return $this->redirectToRoute('admin_page');

          return $this->render('view/admin-page.html.twig', array("queries" => $last_searches, "total" => count($total), "pages" => $pages_array));

        }else{
          return $this->redirectToRoute('admin_login');
        }
      }else{
        return $this->redirectToRoute('admin_login');
    }
  }

}
