<?php

namespace App\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgentRepository")
 */
class Agent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $query;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $ip;


    //Gettery & settery

    public function getId(){
      return $this->id;
    }

    public function getQuery(){
      return $this->query;
    }

    public function setQuery($query){
      $this->query = $query;
    }

    public function getDate(){
      return $this->date;
    }

    public function setDate($date){
      $this->date = $date;
    }

    public function getIp(){
      return $this->ip;
    }

    public function setIp($ip){
      $this->ip = $ip;
    }

}
