<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text", length=50)
     */
    private $user;

    /**
     * @ORM\Column(type="text", length=255)
     */
    private $pass;

    public function getUser(){
      return $this->user;
    }

    public function getPass(){
      return $this->pass;
    }


}
