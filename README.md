**GitAgent**

GitAgent is quick online tool for searching & listing git repositories from individual users.

---

## How to install

1. Pull GitAgent into **htdocs** folder of your webserver.
2. Open phpmyadmin and import **gitagent.sql**.
3. Edit file **.env** on line 23 to match your MySQL settings.
4. You can change your admin password by updating user table and encoding your password + **git4g3nt** (ex. admingit4g3nt).


---

## Usage

Homepage is pretty simple, on the top of the page there's navigation bar with search box, type in any user from **GitHub**, the page will redirect you to the search page where you can browse his public repositories. 
If you search for non-existing username gitagent will print error message, however sometimes when you type in username which is no longer existing (atleast I think thats the cause of it)  it bugs out, shows blank page and doesn't show any error message.
